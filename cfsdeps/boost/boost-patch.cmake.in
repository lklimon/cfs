# -*- mode: cmake; coding: utf-8; indent-tabs-mode: nil; -*-
# kate: indent-width 2; encoding utf-8; auto-brackets on;
# kate: mixedindent on; indent-mode cstyle; line-numbers on;
# kate: syntax cmake; replace-tabs on; background-color #D1EBFF;
# kate: remove-trailing-space on; bracket-highlight-color #ff00ff;

#=============================================================================
# Set global variables, configured from the external build.
#=============================================================================
SET(CFS_SOURCE_DIR "@CFS_SOURCE_DIR@")
SET(CMAKE_CXX_COMPILER_ID "@CMAKE_CXX_COMPILER_ID@")
SET(CMAKE_CXX_COMPILER_VERSION "@CMAKE_CXX_COMPILER_VERSION@")
SET(WIN32 "@WIN32@")
SET(MSVC "@MSVC@")

#=============================================================================
# Include some convenient macros (e.g. for applying patches).
#=============================================================================
INCLUDE("${CFS_SOURCE_DIR}/cmake_modules/CFS_macros.cmake")
# cmake_print_variables(CMAKE_CXX_COMPILER_ID CMAKE_CXX_COMPILER_VERSION)
#=============================================================================
# Apply some patches.
#=============================================================================
IF(WIN32)
  IF(CMAKE_CXX_COMPILER_ID STREQUAL "Intel")
    SET(patches
      "boost-win-icc-alternative-tokens.patch"
    )
    IF(CMAKE_CXX_COMPILER_VERSION VERSION_GREATER_EQUAL 20.1)
      SET(patches
        "boost-win-icc-alternative-tokens.patch"
        "boost-win-icc-oneApi.patch"
      )
    ENDIF()
  ENDIF()
  # Required due to MSVC versioning in boost, therefore patch must be applied
  IF("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC" AND CMAKE_CXX_COMPILER_VERSION VERSION_GREATER_EQUAL "19.40")
	# message("String is equal MSVC, applying patch")
    LIST(APPEND patches "msvc340fix.patch")
  ENDIF()
else()
  set(patches "boost-musl-pre1.79.patch") # required by MUSL clib based system, fixed in boost 1.79: https://github.com/boostorg/interprocess/pull/162
ENDIF()

APPLY_PATCHES("${patches}" "${CFS_SOURCE_DIR}/cfsdeps/boost")
