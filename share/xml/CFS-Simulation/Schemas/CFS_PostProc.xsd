<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema"
  targetNamespace="http://www.cfs++.org/simulation"
  xmlns="http://www.cfs++.org/simulation"
  xmlns:cfs="http://www.cfs++.org/simulation"
  elementFormDefault="qualified">

  <xsd:annotation>
    <xsd:documentation xml:lang="en">
      Coupled Field Solver project CFS++
      Schema description for postprocessing procedures for PDE
      and coupling results
    </xsd:documentation>
  </xsd:annotation>

  <!-- ******************************************************************** -->
  <!--   Definition of basic postprocessing data type -->
  <!-- ******************************************************************** -->
  <xsd:complexType name="DT_PostProcBasic" abstract="true">
    <xsd:attribute name="outputIds" type="xsd:token" default=""/>
    <xsd:attribute name="writeResult" type="DT_CFSBool" default="yes"/>
    <xsd:attribute name="postProcId" type="xsd:token" default=""/>
  </xsd:complexType>
    

  <!-- ******************************************************************** -->
  <!--   Definition of basic postprocessing element -->
  <!-- ******************************************************************** -->
  
  <!-- This element is abstract in order to force substitution -->
  <!-- by the derived specialised PDE elements -->
  <xsd:element name="PostProcBasic" type="DT_PostProcBasic"
  abstract="true"/>


  
  <!-- ******************************************************************** -->
  <!--   Definition of postprocessing list data type -->
  <!-- ******************************************************************** -->
  <xsd:complexType name="DT_PostProcList">
    <xsd:sequence>
      <xsd:element name="postProc" minOccurs="0" maxOccurs="unbounded">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element ref="PostProcBasic" minOccurs="0"
              maxOccurs="unbounded"/>
          </xsd:sequence>
          <xsd:attribute name="id" type="xsd:token"/>
        </xsd:complexType>
      </xsd:element>
    </xsd:sequence>
  </xsd:complexType>

  
  <!-- ******************************************************************** -->
  <!--   Definition of postprocessing operation 'sum' -->
  <!-- ******************************************************************** -->
  <xsd:element name="sum" type="DT_PostProcSum"
       substitutionGroup="PostProcBasic"/>


  <!-- ******************************************************************** -->
  <!--   Definition of data type for postprocessing operation 'sum'  -->
  <!-- ******************************************************************** -->
  <xsd:complexType name="DT_PostProcSum">
    <xsd:complexContent>
      <xsd:extension base="DT_PostProcBasic">
        <xsd:attribute name="reduction" use="optional"
          default="space">
          <xsd:simpleType>
            <xsd:restriction base="xsd:token">
              <xsd:enumeration value="time"/>
              <xsd:enumeration value="space"/>
            </xsd:restriction>
          </xsd:simpleType>
        </xsd:attribute>
       
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>

  <!-- ******************************************************************** -->
  <!--   Definition of postprocessing operation 'max' -->
  <!-- ******************************************************************** -->
  <xsd:element name="max" type="DT_PostProcMax"
       substitutionGroup="PostProcBasic"/>


  <!-- ******************************************************************** -->
  <!--   Definition of data type for postprocessing operation 'max'  -->
  <!-- ******************************************************************** -->
  <xsd:complexType name="DT_PostProcMax">
    <xsd:complexContent>
      <xsd:extension base="DT_PostProcBasic">
        <xsd:attribute name="reduction" use="optional"
          default="space">
          <xsd:simpleType>
            <xsd:restriction base="xsd:token">
              <xsd:enumeration value="time"/>
              <xsd:enumeration value="space"/>
            </xsd:restriction>
          </xsd:simpleType>
        </xsd:attribute>
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>

  <!-- ******************************************************************** -->
  <!--   Definition of postprocessing operation 'func' -->
  <!-- ******************************************************************** -->
  <xsd:element name="function" type="DT_PostProcFunc"
    substitutionGroup="PostProcBasic"/>


  <!-- ******************************************************************** -->
  <!--   Definition of data type for postprocessing operation 'func'  -->
  <!-- ******************************************************************** -->
  <xsd:complexType name="DT_PostProcFunc">
    <xsd:complexContent>
      <xsd:extension base="DT_PostProcBasic">
        <xsd:sequence>
          <xsd:element name="inputResult" minOccurs="1" maxOccurs="1"> <!-- Currently only one input is allowed, but this could be extended-->
            <xsd:complexType>
              <xsd:attribute name="inputId" type="xsd:token" use="prohibited"/>  <!-- This is a placeholder for later usage when multiple inputs can be used in one postProc result -->
              <xsd:attribute name="variableName" type="xsd:token" use="required">  
                <xsd:annotation>
                  <xsd:documentation>
                    Variable name to access the DoFs of the result to be processed.
                    The variables have to be unique for one result and can't coincide
                    with global names like "t", "f", etc. as well as defined variables
                    in the variableList.
                    Example using "u" as variableName:
                    For harmonic computations, access the real and imaginary part with
                    "u_real" and "u_imag", respectively.
                    For transient computations, access scalar values with "u" and
                    vectorial ones with "ux", "uy" and "uz", respectively.
                  </xsd:documentation>
                </xsd:annotation>
              </xsd:attribute>                  
            </xsd:complexType>
          </xsd:element>
          <xsd:choice>
            <xsd:element name="scalar" minOccurs="1" maxOccurs="1">
              <xsd:complexType>
                <xsd:attribute name="realFunc" type="xsd:token" default=""/>
                <xsd:attribute name="imagFunc" type="xsd:token" default=""/>
              </xsd:complexType>
            </xsd:element>
            <xsd:element name="vector" minOccurs="1" maxOccurs="1">
              <xsd:complexType>
                <xsd:sequence>
                  <xsd:element name="comp" minOccurs="1" maxOccurs="unbounded">
                    <xsd:complexType>
                      <xsd:attribute name="dof" type="xsd:token" use="required"/>
                      <xsd:attribute name="realFunc" type="xsd:token" default=""/>
                      <xsd:attribute name="imagFunc" type="xsd:token" default=""/>
                    </xsd:complexType>
                  </xsd:element>
                </xsd:sequence>
              </xsd:complexType>
            </xsd:element>
          </xsd:choice>
        </xsd:sequence>
        <xsd:attribute name="resultName" type="xsd:token" use="required"/>
        <xsd:attribute name="unit" type="xsd:token"
        use="optional" default=""/>
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>
  
  <!-- ******************************************************************** -->
  <!--   Definition of postprocessing operation 'L2Norm' -->
  <!-- ******************************************************************** -->
  <xsd:element name="L2Norm" type="DT_PostProcL2Norm"
    substitutionGroup="PostProcBasic">
    <xsd:annotation>
      <xsd:documentation>
        Evaluates the function \sqrt{ \int_\Omega (u_num-u_real)^2 d\Omega } in absolute mode or the function \sqrt{ \frac{\int_\Omega (u_num-u_real)^2 d\Omega}{\int_\Omega u_real^2 d\Omega} } in relative mode.
        Since the integral gets evaluated numerically but the real solution is given analytically, a suitable (high enough) integration order has to be chosen.
      </xsd:documentation>
    </xsd:annotation>
  </xsd:element>
  

  <!-- ******************************************************************** -->
  <!--   Definition of data type for postprocessing operation 'L2Norm'  -->
  <!-- ******************************************************************** -->
  <xsd:complexType name="DT_PostProcL2Norm">
    <xsd:complexContent>
      <xsd:extension base="DT_PostProcBasic">
        <xsd:choice>
          <xsd:sequence>
            <xsd:element name="dof" minOccurs="1" maxOccurs="unbounded">
              <xsd:complexType>
                <xsd:attribute name="name" type="xsd:token" use="required"/>
                <xsd:attribute name="realFunc" type="xsd:token" default=""/>
                <xsd:attribute name="imagFunc" type="xsd:token" default=""/>
              </xsd:complexType>
            </xsd:element>
          </xsd:sequence>
        </xsd:choice>
        <xsd:attribute name="resultName" type="xsd:token" use="required"/>
        <xsd:attribute name="integrationOrder" type="xsd:integer" use="required"/>
        <xsd:attribute name="mode" use="required">
          <xsd:simpleType>
            <xsd:restriction base="xsd:token">
              <xsd:enumeration value="absolute"/>
              <xsd:enumeration value="relative"/>
            </xsd:restriction>
          </xsd:simpleType>
        </xsd:attribute>
        <xsd:attribute name="computeSolutionSteps" use="optional" default="all">
          <xsd:annotation>
            <xsd:documentation>
              Define custom steps in which the L2 norm is computed.
              "all" (default) ... the L2 norm is computed in every step
              "last"          ... the L2 norm is computed only in the last step
            </xsd:documentation>
          </xsd:annotation>
          <xsd:simpleType>
            <xsd:restriction base="xsd:token">
              <xsd:enumeration value="all"/>
              <xsd:enumeration value="last"/>
            </xsd:restriction>
          </xsd:simpleType>
        </xsd:attribute>
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>

  <!-- ******************************************************************** -->
  <!--   Definition of postprocessing operation 'limit' -->
  <!-- ******************************************************************** -->
  <xsd:element name="limit" type="DT_PostProcLimit"
       substitutionGroup="PostProcBasic"/>


  <!-- ******************************************************************** -->
  <!--   Definition of data type for postprocessing operation 'limit'  -->
  <!-- ******************************************************************** -->
  <xsd:complexType name="DT_PostProcLimit">
    <xsd:complexContent>
      <xsd:extension base="DT_PostProcBasic">
        <xsd:attribute name="lowerLimit" type="xsd:float"
        use="optional" /> 
        <xsd:attribute name="upperLimit" type="xsd:float"
        use="optional" /> 
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>
</xsd:schema>